
CREATE TABLESPACE tbs_perm_01
  DATAFILE 'tbs_perm_01.dat' 
    SIZE 200M
  ONLINE;

CREATE TEMPORARY TABLESPACE tbs_temp_01
  TEMPFILE 'tbs_temp_01.dbf'
    SIZE 50M
    AUTOEXTEND ON;

CREATE USER LPD
  IDENTIFIED BY 123456
  DEFAULT TABLESPACE tbs_perm_01
  TEMPORARY TABLESPACE tbs_temp_01
  QUOTA 200M on tbs_perm_01;
  
  
GRANT create session TO LPD;
GRANT create table TO LPD;
GRANT create view TO LPD;
GRANT create any trigger TO LPD;
GRANT create any procedure TO LPD;l
GRANT create sequence TO LPD;
GRANT create synonym TO LPD;
  

    
    
    
    
  